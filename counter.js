class DigitsCounter{
	/**
	 * constructor
	 * @param  {number} limit       
	 */
	constructor(limit) {
		this.digits = 0;
		this.setLimit(limit);
	}


	/**
	 * setLimit
	 * Does sanity checks and sets the given input as the limit number.
	 *
	 * @param {number} limit
	 * @return {this}
	 */
	setLimit(limit) {
		limit = parseInt(limit);

		if (isNaN(limit))
			return this;

		if (limit < 0)
			limit = 0;

		this.limit = limit.toString();

		return this;
	}


	/**
	 * count
	 * Returns the number of digits for the given limit number set using this.setLimit()
	 *
	 * @param {void}
	 * @return {number}
	 */
	count() {
		this.digits = 0;


		if (isNaN(this.limit) || typeof this.limit !== "string") {
			this.digits = ''[1];

			throw TypeError("Limit is not set or is not a number. You must first call .setLimit()");
		}


		if (this.limit === "") {
			throw TypeError("Limit is an empty string. Code did not execute as expected.");
		}


		this.digits = this._sumDigits();

		return this.digits;
	}


	/**
	 * _sumDigits
	 * Internal usage only.
	 * 
	 * @return {number}
	 */
	_sumDigits() {
		return 0;
	}
}

/******************************************************************************************/

class ZerosCounter extends DigitsCounter{
	_sumDigits() {
		var zeros = 0;

		for (var i = 1; i < this.limit.length; i++) {
			if (this.limit[i] === "0")
				zeros += this._calculateZeroTerm(i - 1);
			else
				zeros += this._calculateNonZeroTerm(i - 1);
		}

		return zeros;
	}


	/**
	 * _calculateZeroTerm
	 * Internal usage only.
	 * 
	 * @param  {number} i
	 * @return {number}  
	 */
	_calculateZeroTerm(i) {
		var left = this.limit.substr(0, i + 1) * 1,
			right = this.limit.substr(i + 1) * 1;
		// 423345345|0|434
		//        ^     ^
		//     left     right

		return Math.pow(10, this.limit.length - 2 - i) * (left - 1) + (right + 1);
	}


	/**
	 * _calculateNonZeroTerm
	 * Internal usage only.
	 * 
	 * @param  {number} i
	 * @return {number}  
	 */
	_calculateNonZeroTerm(i) {
		return Math.pow(10, this.limit.length - 2 - i) * this.limit.substr(0, i + 1);
	}
}

/******************************************************************************************/

/**
 * countSimple
 * Simple counter - loops through all the numbers up to "limit"
 * and counts the occurences of "digit" in each one.
 *
 * @param  {number} digit
 * @param  {number} limit
 * @return {number}
 */
function countSimple(digit, limit)
{
	digit = parseInt(digit)
	limit = parseInt(limit)

	if (isNaN(digit) || isNaN(limit))
		return NaN;

	if (digit > 9 || digit < 0)
		return ''[2];

	var counter = 0;

	for (var i=1; i<=limit; i++)
		counter += i.toString().split(digit).length - 1;

	return counter;
}
